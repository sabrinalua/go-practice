package inter

// House ..
// Create a struct house with variables noRooms, price and city
type House struct {
	noRooms int
	price   float64
	city    string
}

/* struct vs class:
structs:
	- value type
	- stored in stack
class"
	- reference type
	- stored in heap
*/

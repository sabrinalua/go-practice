package inter

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// Random generates a number between min and max
func Random(min int, max int) int {
	return rand.Intn(max-min) + min
}

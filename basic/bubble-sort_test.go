package basic_test

import (
	"fmt"
	"sort"
	"testing"

	"gitlab.com/sabrinalua/go-practice/basic"
	"gotest.tools/assert"
)

func Test_BubbleSort(t *testing.T) {
	input := []int{9, 1, 15, 2, 6}
	out := basic.BubbleSort(input)
	fmt.Println(out)

	assert.Equal(t, true, sort.IntsAreSorted(out))

	fmt.Println("bs2", basic.BubbleSort2([]int{9, 1, 15, 2, 6}))
}

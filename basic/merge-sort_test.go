package basic_test

import (
	"fmt"
	"sort"
	"testing"

	"gitlab.com/sabrinalua/go-practice/basic"
	"gotest.tools/assert"
)

func Test_MergeSort(t *testing.T) {
	// test := []int{9, 1, 15, 2, 6, 8, 16, 62, 5}
	test := []int{9, 1, 15, 2, 6}

	fmt.Println(basic.Mergesort(test))
	assert.Equal(t, true, sort.IntsAreSorted(basic.Mergesort(test)))
}

package basic

// Reverse ... optimized impl of Reverse2 reverses a string
func Reverse(input string) string {
	r := []rune(input)
	reverseRune(r)
	return string(r)
}

func reverseRune(r []rune) {
	//technically, need to swap halfway only
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
}

// Reverse2 simplified implementation to reverse a string
func Reverse2(input string) string {
	r := []rune(input)

	for i, j := 0, len(r)-1; i < j; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

// IsPalindrome check if a string is a palindrome
func IsPalindrome(input string) bool {
	if reversed := Reverse(input); reversed == input {
		return true
	}
	return false
}

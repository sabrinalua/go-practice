package basic_test

import (
	"fmt"
	"testing"

	"gitlab.com/sabrinalua/go-practice/basic"
	"gotest.tools/assert"
)

func Test_Reverse(t *testing.T) {
	ball := "ball"
	reversed, reversed2 := basic.Reverse(ball), basic.Reverse2(ball)
	assert.Equal(t, "llab", reversed)
	assert.Equal(t, "llab", reversed2)

	r2 := basic.Reverse(`The quick brown 狐 jumped Óver the lazy 犬`)
	fmt.Println(r2)
	fmt.Println(basic.Reverse2(`The quick brown 狐 jumped Óver the lazy 犬`))
}

func Test_IsPalindrome(t *testing.T) {
	assert.Equal(t, false, basic.IsPalindrome("apple"))
	assert.Equal(t, true, basic.IsPalindrome("alla"))
}

package basic

/*BubbleSort implementation
Bubble Sort is the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in wrong order.
@params input slice of integers
*/
func BubbleSort(input []int) []int {
	for i := len(input); i > 0; i-- {
		for j := 1; j < i; j++ {
			if input[j-1] > input[j] {
				input[j-1], input[j] = input[j], input[j-1]
			}
		}
	}

	return input
}

// BubbleSort2 ..
func BubbleSort2(input []int) []int {
	for i := 1; i < len(input); i++ {
		if input[i] > input[i-1] {
			input[i], input[i-1] = input[i-1], input[i]
		}
	}
	return input
}

package basic

// Mergesort ...
func Mergesort(in []int) []int {
	if len(in) == 1 {
		return in
	}
	half := len(in) / 2

	a, b := in[:half], in[half:]
	a, b = Mergesort(a), Mergesort(b)
	c := merge(a, b)
	return c
}

// pop from queue (FIFO)
func pop(in []int) (int, []int) {
	return in[0], in[1:]
}

// pop from stack /LIFO 
func stackPop(in [int])(int, []int){
	return in[len(in)-1], in[:len(in)-1]
}

func merge(a, b []int) []int {
	var c []int
	for {
		// var popped int
		if x, y := len(a), len(b); x == 0 || y == 0 {
			break
		}
		var popped int
		if a[0] < b[0] {
			popped, a = pop(a)
			c = append(c, popped)
			continue
		}
		popped, b = pop(b)
		c = append(c, popped)

	}
	for j := 0; j < len(a); j++ {
		c = append(c, a[j])
	}
	for k := 0; k < len(b); k++ {
		c = append(c, b[k])
	}
	return c

}

module gitlab.com/sabrinalua/go-practice

go 1.12

require (
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gotest.tools v2.2.0+incompatible
)

package model

import "errors"

/*Stack type of Stack
stack is LIFO
*/
type Stack []interface{}

// NewStack returns a new stack
func NewStack() Stack {
	return []interface{}{}
}

// Push adds members to the stack
func (me *Stack) Push(inputs ...interface{}) {
	*me = append(*me, inputs...)
}

// Pop pops the stack
func (me *Stack) Pop() (interface{}, error) {
	if me.Length() == 0 {
		return nil, errors.New("empty stack")
	}
	pop := (*me)[me.Length()-1]
	*me = (*me)[:me.Length()-1]
	return pop, nil
}

// Length returns length of stack
func (me *Stack) Length() int {
	return len(*me)
}

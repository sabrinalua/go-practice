package model_test

import (
	"fmt"
	"testing"

	"gitlab.com/sabrinalua/go-practice/model"
)

func Test_Stack(t *testing.T) {
	var stack model.Stack
	stack.Push(2, 1, "a", "b")

	for stack.Length() > 0 {
		x, _ := stack.Pop()
		fmt.Println(x, stack)
	}
}

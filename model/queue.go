package model

// Queue is a fifo struct
type Queue []interface{}

// Push pushes input to queue
func (me *Queue) Push(input ...interface{}) {
	*me = append(me, input...)
}
